import requests
import random # only used to generate example data
import time

# credentials for belfast.pvos.org (for this particular sensor feed)
public_key = "i5dugmfczdw7"
private_key = "5yswadvakbuz"

# these will stay fixed:
base_url = "http://bayou.pvos.org/data/"
#base_url = "http://192.168.1.163:5000/data/"
full_url = base_url+public_key

# start in belfast harbor
current_lat = 44.42849861396812
current_lon = -69.00385950557286

for i in range(0,5):
    current_lat = current_lat + random.randint(1,10)/1000.
    current_lon = current_lon + random.randint(1,10)/1000.
    rssi = random.randint(30,100)*-1
    myobj = {"private_key":private_key, "gps_lat":current_lat, "gps_lon":current_lon, "rssi":rssi}

    x = requests.post(full_url, data = myobj)
    print(myobj)
    print(x.text)

    time.sleep(1)