var express = require('express');
var router = express.Router();
var path = require("path");
var fs = require('fs');
/*
router.get('/', function(req, res, next) {
  res.send('Welcome to Habitat');
});
*/


/* router.get('/', function(req,res, next){
  //res.sendFile(path.join(__dirname,'../public/form.html')); //make this more robust?
  res.render('landing');
}); */

router.get('/', function(req,res, next){
    //res.sendFile(path.join(__dirname,'../public/co2_form.html')); //make this more robust?
    //res.render('landing');
    res.render('create_feed');
  });

router.get('/how-bayou-works/', function(req,res, next){
res.render('bayou_explained');
    });

router.get('/about/', function(req,res, next){
    res.render('about');
      });

router.get('/loracam/',function(req,res,next){
    var files = fs.readdirSync('public/images/loracam/');
    res.render('loracam',{files:files});
});


module.exports = router;
