


function setActiveIcon(marker) {
    marker.setIcon(divIconActive);
  };


  
function makeImageMap(docid,map_url,feeds,colors)
{
    //console.log(feeds);
    console.log("map colors:",colors);


    var map = L.map(docid, {
        minZoom: 1,
        maxZoom: 4,
        center: [0, 0],
        zoom: 3,
        crs: L.CRS.Simple
      });

      var w = 725,
      h = 481;
      var url = map_url;

      //var url = "/images/a2floor.png";

      var southWest = map.unproject([0, h], map.getMaxZoom()-1);
          var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
          var bounds = new L.LatLngBounds(southWest, northEast);
      
          // add the image overlay, 
          // so that it covers the entire map
          L.imageOverlay(url, bounds).addTo(map);
      
          // tell leaflet that the map is exactly as big as the image
          map.setMaxBounds(bounds);
      
       console.log('got here');

       var locationLayer = new L.FeatureGroup();
       var markerTemp = L.marker();
     
       //var markerData = [];
       for(var i = 0; i < feeds.length; i++) {
           var feed_shortkey = feeds[i].feed_pubkey.substring(0,4);
           var x = feeds[i].coords.x;
           var y = feeds[i].coords.y;
           var coords = feeds[i].coords;
           var shortkey = feeds[i].name;
           console.log(shortkey);

           var htmlString = "<div style='background-color:white;' class='marker-pin'></div><i class='material-icons' style='color:"+colors[i]+"'><b>\""+shortkey.toString()+"\"</b></i>";
           //markerData.push({"feed_shortkey":feed_shortkey,"coords":[x,y]});

           var icon = L.divIcon({
            className: 'custom-div-icon',
            html: htmlString,
            iconSize: [10, 42],
            iconAnchor: [15, 42]
            });

            console.log(x,y);

            var marker = L.marker([x,y], {
                //icon: divIcon,
                icon: icon,
                id: i
              });

            marker.addTo(map);
           
        }   

}
    
    
    function makeChart(docid,bdata,param_key)
{
    //console.log(param_key);

    
    var ctx = document.getElementById(docid).getContext('2d');

    //resize canvas

    var param_vs_time = [];
            for(var i = 0; i < bdata.length; i++) {
            var thisco2 = bdata[i].parameters[param_key];
            var timeutc = bdata[i].timestamp;
            var localtime = luxon.DateTime.fromISO(timeutc).toLocal().toString();
            param_vs_time.push({"t":localtime,"y":thisco2})
            }
    
    console.log(param_vs_time);

    var chart = new Chart(ctx, {
        type: 'line',
        data: {
        datasets: [{
        label: param_key,
        lineTension: 0,
        bezierCurve: false,
        fill: true,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: param_vs_time
        }]
        },
        // Configuration options go here
        options: {
        scales: {
        xAxes: [{
        type: 'time',
        distribution: 'linear',
        ticks: {
        major: {
        enabled: true, // <-- This is the key line
        fontStyle: 'bold', //You can also style these values differently
        fontSize: 14, //You can also style these values differently
        },
        },
        }],
        },
        zone: "America/NewYork"
        }
        });
}

function makeChartOverlay(docid,bayoudata,param_key,colors)
{
    console.log("chart colors:",colors);

    //console.log(param_key);
    console.log(bayoudata.length);
    console.log(bayoudata);


    var bdata=bayoudata[0];

    var ctx = document.getElementById(docid).getContext('2d');

    //resize canvas
    var datasets =[];
    //var colors = ['red','green','blue','purple','yellow'];
    var feedcount=0;

    bayoudata.forEach(feed =>
    {
        //var feed = element.data;
        var feed_pubkey = feed.feedjson.feed_pubkey;
        var feed_nickname = feed.feed_nickname;
        var feed_data = feed.feedjson.data;

        console.log("feed_pubkey:", feed_pubkey);
        console.log("feed_nickname:",feed_nickname);

        var param_vs_time = [];
        for(var i = 0; i < feed_data.length; i++) {
            var thisco2 = feed_data[i].parameters[param_key];
            var timeutc = feed_data[i].timestamp;
            var localtime = luxon.DateTime.fromISO(timeutc).toLocal().toString();
            param_vs_time.push({"t":localtime,"y":thisco2});
        }
        
        var feed_shortname = "\""+feed_pubkey.substr(0,3)+"\"";
        var chartcolor = colors[feedcount];
        console.log(chartcolor);
        var dataset = {
            "data":param_vs_time,
            "label":feed_nickname,
            //"label":feed_shortname,
            "borderColor": chartcolor,
            "fill":true,
            //"spanGaps":false,
            //"showLine":false,
            "lineTension":0
        }
        datasets.push(
            dataset
        );
        feedcount++;
    });    

    var chart = new Chart(ctx, {
        type: 'line',
        data: {
        datasets: datasets
        },
        // Configuration options go here
        options: {
            /*title: {
                display: true,
                text: param_key
            },*/
            legend: {
                labels: {
                    fontStyle: 'bold', //You can also style these values differently
                }
            },
        scales: {
        xAxes: [{
        type: 'time',
        distribution: 'linear',
        ticks: {
        major: {
        enabled: true, // <-- This is the key line
        fontStyle: 'bold', //You can also style these values differently
        fontSize: 14, //You can also style these values differently
        },
        },
        }],
        yAxes: [{
            display:true,
            labelString: param_key
        }]
        },
        zone: "America/NewYork"
        }
        });
}


function makeNetwork(bayoudata,colors) {

    var w = 1000;
    var h = 600;
    var linkDistance=200;

    var colors = d3.scale.category10();

    
    var bayou_nodes =[];
    var bayou_edges = [];
    
    var hopped_to =[];
    var node_list =[];

    bayoudata.forEach(feed =>
    {
        var feed_pubkey = feed.feedjson.feed_pubkey;
        var feed_nickname = feed.feed_nickname;
        var feed_data = feed.feedjson.data;

        var next_hop=feed_data[feed_data.length-1].parameters.next_hop;
        var node_id=feed_data[feed_data.length-1].parameters.node_id;
        var next_rssi=feed_data[feed_data.length-1].parameters.next_rssi;

        bayou_nodes.push({name:node_id+": "+feed_nickname,node_id:node_id,nickname:feed_nickname});
        
        //console.log(feed_data[feed_data.length-1].parameters)
        console.log("next_hop actual:",feed_data[feed_data.length-1].parameters.next_hop)
        console.log("next_hop:",parseInt(next_hop));

        console.log("node_id actual:",feed_data[feed_data.length-1].parameters.node_id)
        console.log("node_id:",parseInt(node_id));

        if(parseInt(next_hop)>0) {
            bayou_edges.push({source:parseInt(node_id)-1, target:parseInt(next_hop)-1,rssi:parseInt(next_rssi)});
            hopped_to.push(parseInt(next_hop));
            node_list.push(parseInt(node_id));
        }
        

    });
    
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    var hopped_to_unique = hopped_to.filter(onlyUnique);

    /*
    hopped_to_unique.sort(function(a, b) {
        return parseInt(a) - parseInt(b);
    });

    hopped_to_unique.sort();
    */

    console.log("hopped_to_unique:",hopped_to_unique);
    hopped_to_unique.forEach(dest =>
        {
            //console.log("n:",node);
            if (node_list.includes(dest)==false) {
                console.log("repeater:",dest);
                bayou_nodes.push({name:dest+": (repeater)",node_id:dest,nickname:"ghost"});
            }
        })

    bayou_nodes.sort(function(a, b) {
            return parseInt(a.node_id) - parseInt(b.node_id);
    });
    
    //bayou_nodes.sort();

    console.log("bayou_nodes:",bayou_nodes);
    console.log("bayou_edges:",bayou_edges);
    console.log("hopped_to:",hopped_to);
    console.log("node_list:",node_list);

    var dataset = {

    nodes:bayou_nodes,
    edges: bayou_edges
    };

 
    var svg = d3.select("body").append("svg").attr({"width":w,"height":h});

    var force = d3.layout.force()
        .nodes(dataset.nodes)
        .links(dataset.edges)
        .size([w,h])
        .linkDistance([linkDistance])
        .charge([-500])
        .theta(0.1)
        .gravity(0.05)
        .start();

 

    var edges = svg.selectAll("line")
      .data(dataset.edges)
      .enter()
      .append("line")
      .attr("id",function(d,i) {return 'edge'+i})
      .attr('marker-end','url(#arrowhead)')
      .style("stroke","#ccc")
      .style("pointer-events", "none");
    
    var nodes = svg.selectAll("circle")
      .data(dataset.nodes)
      .enter()
      .append("circle")
      .attr({"r":15})
      .style("fill",function(d,i){return colors(i);})
      .call(force.drag)


    var nodelabels = svg.selectAll(".nodelabel") 
       .data(dataset.nodes)
       .enter()
       .append("text")
       .attr({"x":function(d){return d.x;},
              "y":function(d){return d.y;},
              "class":"nodelabel",
              "stroke":"black"})
       .text(function(d){return d.name;});

    var edgepaths = svg.selectAll(".edgepath")
        .data(dataset.edges)
        .enter()
        .append('path')
        .attr({'d': function(d) {return 'M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y},
               'class':'edgepath',
               'fill-opacity':0,
               'stroke-opacity':0,
               'fill':'blue',
               'stroke':'red',
               'id':function(d,i) {return 'edgepath'+i}})
        .style("pointer-events", "none");

    var edgelabels = svg.selectAll(".edgelabel")
        .data(dataset.edges)
        .enter()
        .append('text')
        .style("pointer-events", "none")
        .attr({'class':'edgelabel',
               'id':function(d,i){return 'edgelabel'+i},
               'dx':80,
               'dy':0,
               'font-size':13,
               'stroke':'red',
               'fill':'#aaa'});

    edgelabels.append('textPath')
        .data(dataset.edges)
        .attr('xlink:href',function(d,i) {return '#edgepath'+i})
        .style("pointer-events", "none")
        //.text(function(d,i){return 'label '+i});
        .text(function(d){return d.rssi;});

    svg.append('defs').append('marker')
        .attr({'id':'arrowhead',
               'viewBox':'-0 -5 10 10',
               'refX':25,
               'refY':0,
               //'markerUnits':'strokeWidth',
               'orient':'auto',
               'markerWidth':10,
               'markerHeight':10,
               'xoverflow':'visible'})
        .append('svg:path')
            .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
            .attr('fill', '#ccc')
            .attr('stroke','#ccc');
     

    force.on("tick", function(){

        edges.attr({"x1": function(d){return d.source.x;},
                    "y1": function(d){return d.source.y;},
                    "x2": function(d){return d.target.x;},
                    "y2": function(d){return d.target.y;}
        });

        nodes.attr({"cx":function(d){return d.x;},
                    "cy":function(d){return d.y;}
        });

        nodelabels.attr("x", function(d) { return d.x; }) 
                  .attr("y", function(d) { return d.y; });

        edgepaths.attr('d', function(d) { var path='M '+d.source.x+' '+d.source.y+' L '+ d.target.x +' '+d.target.y;
                                           //console.log(d)
                                           return path});       

        edgelabels.attr('transform',function(d,i){
            if (d.target.x<d.source.x){
                bbox = this.getBBox();
                rx = bbox.x+bbox.width/2;
                ry = bbox.y+bbox.height/2;
                return 'rotate(180 '+rx+' '+ry+')';
                }
            else {
                return 'rotate(0)';
                }
        });
    });

}