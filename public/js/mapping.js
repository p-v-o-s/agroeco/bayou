
function simpleGrid(map) {
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    L.maidenhead({color : 'rgba(255, 0, 0, 0.4)'}).addTo(map);

}


function makeMap(map,feed_pubkey,plot_param,limit,node_id,) {
            var start_lat=42.41180413027933;
            var start_lon=-71.29848143160702;

            //var start_lat=42.42198063922096;
            //var start_lon=-71.14137204377866;
            
            //var start_lat=44.42849861396812;
            //var start_lon=-69.00385950557286;
            
            var ave_lat = 0;
            var ave_lon = 0;
            var ave_count = 0;
            //var map = L.map(docid).setView([start_lat, start_lon], 13);

            // stamen refs: http://maps.stamen.com/#watercolor/
            
            var usual = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
            var cycling = 'https://tile.waymarkedtrails.org/{id}/{z}/{x}/{y}.png';
            var topo = 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png';
            var toner = 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png'
            var terrain = 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg'
            var watercolor = 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg'
            
            var maptype = watercolor;

            L.tileLayer(maptype, {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            }).addTo(map);
            
            
            /*
            var WaymarkedTrails_hiking = L.tileLayer('https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://waymarkedtrails.org">waymarkedtrails.org</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
            }).addTo(map);
            */

            

            L.maidenhead({
                //color : 'rgba(255, 0, 0, 0.4)'
                color : 'rgba(0, 0, 0, 1)'
            }).addTo(map);
            
            //map.setView([42.41013790165315, -71.29854445045332], 17);
            map.setView([42.4102, -71.3037], 15);
            //42.4102/-71.3037
            /*
            const dataurl = '/data/'+feed_pubkey+'/json/'+node_id;

            
            fetch(dataurl)
            .then(response => response.json())
            .then(bdata => {
                var data = bdata.data;
                //console.log(data);

                var coords_vs_time = [];
                for(var i = 0; i < data.length; i++) {
                var timeutc = data[i].timestamp;
                var parameters=data[i].parameters;
                if ((parameters.gps_lat != null) && (parameters.gps_lon != null)) {
                ave_lat=ave_lat+parameters.gps_lat;
                ave_lon=ave_lon+parameters.gps_lon;
                ave_count=ave_count+1;
                var this_gps = [parameters.gps_lat,parameters.gps_lon];
                var this_rssi = parameters.rssi;
                var localtime = luxon.DateTime.fromISO(timeutc).toLocal().toString();
                if (this_rssi == null) this_rssi = 0;
                var this_color = 'red';
                if (this_rssi >= -55) this_color = 'lightgreen';
                if (this_rssi < -65) this_color = 'green';
                if (this_rssi < -70) this_color = 'lightyellow';
                if (this_rssi < -80) this_color = 'yellow';
                if (this_rssi < -85) this_color = 'lightgred';
                if (this_rssi < -90) this_color = 'red';

                coords_vs_time.push({"t":localtime,"coords":this_gps,"rssi":this_rssi, "color":this_color});
                }
                //console.log(luxon.DateTime.fromISO(bdata[i].created));
                }
                console.log(coords_vs_time);
                ave_lat=ave_lat/ave_count;
                ave_lon=ave_lon/ave_count;
                console.log("start",start_lat,start_lon);
                console.log("ave",ave_lat,ave_lon);

                //map.setView([ave_lat, ave_lon], 13); 
                map.setView([start_lat, start_lon], 17);

                for (var j = 0; j < coords_vs_time.length; j++) {
                    L.circle(coords_vs_time[j].coords, 1, {
                        color: coords_vs_time[j].color,
                        fillColor: coords_vs_time[j].color,
                        fillOpacity: 0.8
                    }).addTo(map).bindPopup(coords_vs_time[j].rssi.toString());
                }

                polygonList=[];
                for (var k = 0; k < coords_vs_time.length; k++) {
                   polygonList.push(coords_vs_time[k].coords);
                }
                //console.log(polygonList);

                L.polyline(polygonList).setStyle({weight: 2}).addTo(map);

            });
            */

}