var express = require('express');
var router = express.Router();
//var passport = require('passport')
//var basicAuth = passport.authenticate('basic', { session: false })
var data_feeds = require('../controllers/dataControllers');
var path = require("path");
//const cors = require('cors');

/*
const whitelist = ['http://bayou.pvos.org','http://192.168.1.163:5000']
const corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    },
  }
*/

router.get('/:feed_pubkey/',data_feeds.getPage);

router.get('/:feed_pubkey/ach/:node_id',data_feeds.getFit);

router.get('/:feed_pubkey/json/', data_feeds.getJSON);

router.get('/:feed_pubkey/json/:node_id', data_feeds.getNodeJSON);

router.get('/:feed_pubkey/csv/',data_feeds.getCSV);

router.get('/:feed_pubkey/csv/:node_id',data_feeds.getNodeCSV);

router.get('/:feed_pubkey/map/',data_feeds.getMap);

router.get('/:feed_pubkey/gridsquare/:node_id',data_feeds.getGridsquare);

router.post('/:feed_pubkey/', data_feeds.postNewMeasurement);

router.get('/:feed_pubkey/latest/',data_feeds.getLatestMeasurement);
router.get('/:feed_pubkey/latest/:node_id',data_feeds.getNodeLatestMeasurement);


module.exports = router;