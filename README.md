# bayou

A Node.js server for IoT stuff, backed by a PostgresSQL database. Based on the [Habitat](https://github.com/RhettTrickett/habitat) project as described in Rhett Trickett's blog post [Creating a web app with a Raspberry Pi, Express and PostgreSQL](https://able.bio/rhett/creating-a-web-app-with-a-raspberry-pi-express-and-postgresql--3c90a372)


## Install Node.js
### on Ubuntu Linux and Raspberry Pi OS, most architectures (excluding armv6l like RPi Zero, 1)
Get `nvm` to manage you Node.js distribution

```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```

Close and reopen your terminal session.  Then, install Node.js

```
nvm install node
```
### on armv6l such as RPi Zero, 1

Setup Raspberry Pi OS Lite (no desktop environment), for headless RPi Zero W setup see: https://cdn-learn.adafruit.com/downloads/pdf/raspberry-pi-zero-creation.pdf

Create location for node builds:
```
cd ~
mkdir nodejs
cd nodejs
```
Find the latest version number from https://nodejs.org/en/ and set environment variable, e.g.:
```
export NODE_VER=16.4.2
```
Create and edit file named `install-node.sh`:
```
touch install-node.sh
chmod +x install-node.sh
nano install-node.sh
```
containing the contents (derived from this blog post: https://blog.rodrigograca.com/how-to-install-latest-nodejs-on-raspberry-pi-0-w/):
```bash
if ! node --version | grep -q ${NODE_VER}; then
  (cat /proc/cpuinfo | grep -q "Pi Zero") && if [ ! -d node-v${NODE_VER}-linux-armv6l ]; then
    echo "Installing nodejs ${NODE_VER} for armv6 from unofficial builds..."
    curl -O https://unofficial-builds.nodejs.org/download/release/v${NODE_VER}/node-v${NODE_VER}-linux-armv6l.tar.xz
    tar -xf node-v${NODE_VER}-linux-armv6l.tar.xz
  fi
  echo "Adding node to the PATH"
  PATH=$(pwd)/node-v${NODE_VER}-linux-armv6l/bin:${PATH}
  echo "Adding node to the PATH in .bashrc"
  echo "PATH=$(pwd)/node-v${NODE_VER}-linux-armv6l/bin:\${PATH}" >> ~/.bashrc
fi
node --version # or whatever ....
```
Run the script and the correct Node.js should be should be downloaded and installed!
```
./install-node.sh
```

## Seting up postgresql database on Ubuntu or Raspberry Pi OS

### Install postgres

```
sudo apt install postgresql postgresql-contrib
```
Then you will need to install a specific version of Postgres, choose the latest one available.  E.g. on Raspberry Pi OS Lite:

```
sudo apt install postgresql-11
```

### Set password for user postgres


```
sudo -i -u postgres
```
```
psql
```

```
ALTER USER postgres PASSWORD 'myPassword';
```

### Create database 'bayou'


```
sudo -i -u postgres
```

```
createdb bayou
```

### Add tables to database 'bayou'

```
sudo -i -u postgres
```

```
psql bayou
```
```
CREATE TABLE feeds( feed_id SERIAL PRIMARY KEY, name VARCHAR(255), public_key VARCHAR(255) UNIQUE, private_key VARCHAR(255) );
```

```
CREATE TABLE measurements( id SERIAL PRIMARY KEY, feed_id INT, temperature_c FLOAT, humidity_rh FLOAT, co2_ppm FLOAT, light_lux FLOAT, distance_meters FLOAT, pressure_mbar FLOAT, battery_volts FLOAT, rssi FLOAT, gps_lat FLOAT, gps_lon FLOAT, gps_alt FLOAT, node_id INT, next_hop INT, next_rssi FLOAT, distance_meters_1 FLOAT, distance_meters_2 FLOAT, distance_meters_3 FLOAT, temperature_c_1 FLOAT, temperature_c_2 FLOAT, temperature_c_3 FLOAT, voltage_1 FLOAT, voltage_2 FLOAT, voltage_3 FLOAT, aux_1 FLOAT, aux_2 FLOAT, aux_3 FLOAT, log VARCHAR(255),
timestamp TIMESTAMP DEFAULT NOW(), CONSTRAINT feed FOREIGN KEY(feed_id) REFERENCES feeds(feed_id) );
```
### (optional) Add a new column to an existing database 'bayou'

```
sudo -i -u postgres
```

```
psql bayou
```
For example:
```
ALTER TABLE measurements RENAME COLUMN created TO timestamp;
```

## Install and configure the Bayou web server
Clone the `bayou` repository

```
git clone https://gitlab.com/p-v-o-s/agroeco/bayou.git
```
Initialize using `npm`
```
cd bayou
npm init
npm install
```
Configure the server to run locally (on test port e.g. 3000) with database using the `postgres` user account on the default port (5432).  Create and edit the `.env` settings file:
```
touch .env
nano .env
```
Write your settings into the file, e.g.:
```
PORT=3000
DB_NAME=bayou
DB_USER=postgres
DB_PASSWORD=myPassword
DB_HOST=localhost
DB_PORT=5432
BASE_URL=IPaddrOrHostName:Port
```

Start the server running locally:
```
npm start
```
Browse to `localhost:3000`: if the database is configured correctly you should be able to create a new feed!
