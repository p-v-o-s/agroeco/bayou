import requests
import random # only used to generate example data

# credentials for belfast.pvos.org (for this particular sensor feed)
public_key = "7367wv8ujm8b"
private_key = "44g33mgh9xeu"

# these will stay fixed:
base_url = "http://belfast.pvos.org/data/"
#base_url = "http://192.168.1.163:5000/data/"
full_url = base_url+public_key

# example data:
distance = random.randint(50,80)
temp = random.randint(10,30)

# the JSON object we'll be POST-ing to 'full_url' ...
# NOTE: we must include the private_key as one of the parameters;
# and 'distance_meters' is one of several possible parameters in the postgres database.
myobj = {"private_key":private_key, "distance_meters":distance,"temperature_c":temp}

x = requests.post(full_url, data = myobj)
print (distance)
print(x.text)
